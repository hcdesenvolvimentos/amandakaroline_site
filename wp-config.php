<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_amandakaroline_site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3H3ExmV0h;A;XQ x}|wC5lFqg4:62/d@^*Xu.-n~ZgF+B7Geys$>LEKiyW6R|@pN');
define('SECURE_AUTH_KEY',  'gkem6Q5U,%#=f(M6pwZ1Yw2Z)^qM?~m?SZ3KI2.;Dl]PiyU:][3k2#|V_JZ{WP.o');
define('LOGGED_IN_KEY',    'X&9.{^o4X8)hCn47dVjlJ(styM^gJeW9K6(Kc#}-`C|E8] CmS?^%8dfK@rS2c#L');
define('NONCE_KEY',        'M0PjgPe1=1=trSnNI_,*y(p-ZvSD0]%:9h4z#f[y#^y>crN4Z<{?2USvUIZ0bWt(');
define('AUTH_SALT',        'Ev^O)@$ Cp3cB^Spm_l/DMfMEZ_|qESXO`fL 6!Uro3X7bvUto+O1x{f<mFem{[o');
define('SECURE_AUTH_SALT', '3D!xA_}43%q.%aHX{fJwj>V3bNd;!)d@UAa_-y!gV4TK:.Mplve,*a^s2@|!xlJ=');
define('LOGGED_IN_SALT',   'U#* )TV-A;+ 2^C4b@Z}UmHbk@#B9xC=lOUh#elD<`dJ+/w4L%P:.j#>?<Mv#+0Y');
define('NONCE_SALT',       ' GKuokT|zAF^.f_i_il$-T; y,T,LQsyg(!VEHTFSP]0%sQWCr2}pC^dzbYbQl|,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ak_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
