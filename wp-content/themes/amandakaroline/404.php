<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Amanda_Karoline
 */
global $configuracao;
get_header(); // NESSA PAGINA NAO É NECESSARIO O HEADER.
?>
<div class="pg pg-404" style="background: url(<?php echo $configuracao['opt_fundo_404']['url'];  ?>);">
	<div class="logo404" href="<?php echo home_url('/');?>">
			<a href="<?php echo home_url('/');?>">
				<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="">
			</a>
	</div>
	<div class="containerLargura">
		<div class="row">
			<div class="col-sm-6">
				<span class="botao">
					<a href="<?php echo home_url('/');?>">Continue Navegando :)</a>
				</span>
			</div>
			<div class="col-sm-6">
				<small>Página não encontrada.</small>
			</div>
		</div>
	</div>
</div>
